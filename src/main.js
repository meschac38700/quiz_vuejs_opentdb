import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'


Vue.use(BootstrapVue);
Vue.use(IconsPlugin)
Vue.config.productionTip = false

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.filter('html_entity', function(value)
{
    var d = document.createElement("div");
    d.innerHTML = value;
    return d.textContent;
});

new Vue({
  render: h => h(App),
}).$mount('#app')
